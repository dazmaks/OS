ASM_SRC = $(wildcard src/impl/*.asm)
C_SRC   = $(wildcard src/kernel/*.c src/impl/*.c)

ASM     = nasm
CC      = gcc
LD      = ld

CFLAGS  = -Wall -Wextra -m32 --no-pie --freestanding -Isrc/intf
LDFLAGS = -Tsrc/linker.ld --oformat binary

SRC     = ${sort ${dir ${wildcard src/*/}}}
BUILD   = build

OBJ     = $(C_SRC:src/%.c=$(BUILD)/%.o) $(ASM_SRC:src/%.asm=$(BUILD)/%.o)

all: clean format floppy.bin

format:
	@clang-format -i src/*/*.c src/*/*.h

run: floppy.bin
	@qemu-system-i386 -drive format=raw,if=floppy,file=$^

$(BUILD):
	@mkdir -p $(SRC:src/%=$(BUILD)/%)

floppy.bin: boot.bin kernel.bin
	@echo "CAT    $^ > $@"
	@cat $^ > $@

kernel.bin: $(BUILD) ${OBJ}
	@echo "LD     $@"
	@$(LD) $(LDFLAGS) -melf_i386 -o $@ $(OBJ)

boot.bin: src/boot/bootsect.asm
	@echo "NASM   $@"
	@$(ASM) -fbin -o $@ $^

$(BUILD)/%.o: src/%.c
	@echo "CC     $@"
	@$(CC) $(CFLAGS) -c -o $@ $^

$(BUILD)/%.o: src/%.asm
	@echo "NASM   $@"
	@$(ASM) -felf32 -o $@ $^

clean:
	@rm -rf $(BUILD) *.bin
