#ifndef COLOR_H
#define COLOR_H

#include <stdint.h>

typedef enum {
    BLACK,
    DARK_BLUE,
    DARK_GREEN,
    DARK_CYAN,
    DARK_RED,
    MAGENTA,
    BROWN,
    GREY,
    DARK_GREY,
    BLUE,
    GREEN,
    CYAN,
    RED,
    PINK,
    YELLOW,
    WHITE,
} color_t;

uint8_t get_color(const color_t fg, const color_t bg);

#endif
