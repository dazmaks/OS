#ifndef PORTS_H
#define PORTS_H

#include <stdint.h>

uint8_t port_byte_in(const uint16_t);
void port_byte_out(const uint16_t, const uint8_t);

/*
uint16_t port_word_in (const u16);
void port_word_out (const u16, const u16);
*/

#endif
