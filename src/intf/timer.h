#ifndef TIMER_H
#define TIMER_H

#include "isr.h"

void timer_callback(const registers_t);

#endif
