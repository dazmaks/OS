#ifndef INTERRUPTS
#define INTERRUPTS

#include "stdbool.h"

void toggle_interrupts(void);

/**
 * Function to enable or disable interrupts
 * \param status Set status true to enable and false to disable interrupts
 */
void enable_interrupts(const bool);

#endif