#ifndef STRING_H
#define STRING_H

#include <stddef.h>

/**
 * Getting length of a string
 * \param str A char* string to get length of
 * \return Length in size_t type
 */
size_t strlen(const char* str);

/**
 * Getting difference between two strings
 * \param str1 First string
 * \param str2 Second string
 * \return Difference in int type (0 if strings are equal)
 */
int strcmp(const char* str1, const char* str2);

/**
 * Writes exactly n bytes, copying from source or adding nulls
 * \param dst Destanation string
 * \param src Source string
 * \param n How much bytes will strncpy write
 * \return A copy of dst
 */
char* strncpy(char* dst, const char* src, size_t n);

#endif
