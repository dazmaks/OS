#ifndef CURSOR_H
#define CURSOR_H

#include <stddef.h>
#include <stdint.h>

void disable_cursor(void);
void enable_cursor(const uint8_t, const uint8_t);
void update_cursor(const size_t, const size_t);

#endif
