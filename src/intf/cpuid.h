#ifndef CPUID_H
#define CPUID_H

#define cpuid(in, a, b, c, d)                    \
    __asm__("cpuid"                              \
            : "=a"(a), "=b"(b), "=c"(c), "=d"(d) \
            : "a"(in))

char* get_brandstring();

#endif
