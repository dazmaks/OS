#ifndef KEYBOARD_H
#define KEYBOARD_H

#include "isr.h"

/**
 * Handler for PS/2 keyboard input IRQ
 * \param regs Unused register_interrupt_handler registers
 */
void keyboard_callback(const registers_t);

#define KEYBUFFERSIZE 0x100

// https://wiki.osdev.org/PS/2_Keyboard#Scan_Code_Set_1
// Special keys
#define LSHIFT 0x2A
#define RSHIFT 0x36

#define LSHIFTRELEASE 0xAA
#define RSHIFTRELEASE 0xB6

#define LALT 0x38
#define RALT 0xE0
#define LCTRL 0x1D
#define RCTRL 0x0E, 0x1D

#define BACKSPACE 0x0E
#define ENTER 0x1C
#define TAB 0x0F

#define CAPSLOCK 0x3A
#define NUMLOCK 0x45
#define SCROLLLOCK 0x46

#define F1 0x3B
#define F2 0x3C
#define F3 0x3D
#define F4 0x3E
#define F5 0x3F
#define F6 0x40
#define F7 0x41
#define F8 0x42
#define F9 0x43
#define F10 0x44
#define F11 0x57
#define F12 0x58

#define KEY0 0x52
#define KEY1 0x4F
#define KEY2 0x50
#define KEY3 0x51
#define KEY4 0x4B
#define KEY5 0x4C
#define KEY6 0x4D
#define KEY7 0x47
#define KEY8 0x48
#define KEY9 0x49

#define KEYMINUS 0x4A
#define KEYPLUS 0x4E

#define KEYDOT 0x53
#define KEYASTERISK 0x37

#endif
