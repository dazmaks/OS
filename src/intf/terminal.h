#ifndef TERMINAL_H
#define TERMINAL_H

#include <stddef.h>
#include <stdint.h>

#define VGA_COLUMN_MAX 80
#define VGA_ROW_MAX 25
#define TERM_BUFFER_SIZE 0xB8000
#define POSITION y* VGA_COLUMN_MAX + x

extern int8_t term_row;
extern int8_t term_column;
extern uint8_t term_color;
extern uint16_t* term_buffer;

void putentryat(const char, const uint8_t, const size_t, const size_t);

#endif
