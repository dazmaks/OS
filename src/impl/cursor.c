#include <cursor.h>
#include <mem.h>
#include <ports.h>
#include <terminal.h>

void disable_cursor(void)
{
    port_byte_out(0x3D4, 0x0A);
    port_byte_out(0x3D5, 0x20);
}

void enable_cursor(const uint8_t cursor_start, const uint8_t cursor_end)
{
    port_byte_out(0x3D4, 0x0A);
    port_byte_out(0x3D5, (port_byte_in(0x3D5) & 0xC0) | cursor_start);

    port_byte_out(0x3D4, 0x0B);
    port_byte_out(0x3D5, (port_byte_in(0x3D5) & 0xE0) | cursor_end);
}

void update_cursor(const size_t x, const size_t y)
{
    port_byte_out(0x3D4, 0x0F);
    port_byte_out(0x3D5, low_8(POSITION));
    port_byte_out(0x3D4, 0x0E);
    port_byte_out(0x3D5, high_8(POSITION));
}
