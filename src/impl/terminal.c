#include <terminal.h>

int8_t term_row;
int8_t term_column;
uint8_t term_color;
uint16_t* term_buffer;
uint16_t* term_buffer = (uint16_t*)TERM_BUFFER_SIZE;

static inline uint16_t entry(const uint8_t uc, const uint8_t color)
{
    return (uint16_t)uc | (uint16_t)color << 8;
}

void putentryat(const char c, const uint8_t color, size_t x, size_t y)
{
    term_buffer[POSITION] = entry(c, color);
}
