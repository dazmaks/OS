/*
 * Copyright (c) 2006-2007 -  http://brynet.biz.tm - <brynet@gmail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL
 * THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* You need to include a file with fairly(ish) compliant printf prototype,
 * Decimal and String support like %s and %d and this is truely all you need! */
//#include <stdio.h> /* for printf(); */

#include <cpuid.h>
#include <printf.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>
#include <unused.h>

/* Intel Specific brand list */
char* Intel[] = { "Brand ID Not Supported.",
    "Intel(R) Celeron(R) processor",
    "Intel(R) Pentium(R) III processor",
    "Intel(R) Pentium(R) III Xeon(R) processor",
    "Intel(R) Pentium(R) III processor",
    "Reserved",
    "Mobile Intel(R) Pentium(R) III processor-M",
    "Mobile Intel(R) Celeron(R) processor",
    "Intel(R) Pentium(R) 4 processor",
    "Intel(R) Pentium(R) 4 processor",
    "Intel(R) Celeron(R) processor",
    "Intel(R) Xeon(R) Processor",
    "Intel(R) Xeon(R) processor MP",
    "Reserved",
    "Mobile Intel(R) Pentium(R) 4 processor-M",
    "Mobile Intel(R) Pentium(R) Celeron(R) processor",
    "Reserved",
    "Mobile Genuine Intel(R) processor",
    "Intel(R) Celeron(R) M processor",
    "Mobile Intel(R) Celeron(R) processor",
    "Intel(R) Celeron(R) processor",
    "Mobile Geniune Intel(R) processor",
    "Intel(R) Pentium(R) M processor",
    "Mobile Intel(R) Celeron(R) processor" };

/* This table is for those brand strings that have two values depending on the
 * processor signature. It should have the same number of entries as the above
 * table. */
char* Intel_Other[] = { "Reserved",
    "Reserved",
    "Reserved",
    "Intel(R) Celeron(R) processor",
    "Reserved",
    "Reserved",
    "Reserved",
    "Reserved",
    "Reserved",
    "Reserved",
    "Reserved",
    "Intel(R) Xeon(R) processor MP",
    "Reserved",
    "Reserved",
    "Intel(R) Xeon(R) processor",
    "Reserved",
    "Reserved",
    "Reserved",
    "Reserved",
    "Reserved",
    "Reserved",
    "Reserved",
    "Reserved",
    "Reserved" };

void regs2string(int eax, int ebx, int ecx, int edx, char* buf)
{
    char string[17];
    string[16] = '\0';

    for (int j = 0; j < 4; j++) {
        string[j] = eax >> (8 * j);
        string[j + 4] = ebx >> (8 * j);
        string[j + 8] = ecx >> (8 * j);
        string[j + 12] = edx >> (8 * j);
    }
    strncpy(buf, string, 17);
}

char brandstring[50] = { 0 };

char* get_brandstring()
{
    unsigned long eax, ebx, ecx, edx, max_eax, signature, unused;
    int32_t brand;
    cpuid(0x80000000, max_eax, unused, unused, unused);
    cpuid(1, eax, ebx, unused, unused);
    UNUSED(unused);
    brand = ebx & 0xff;
    signature = eax;
    /* Quok said: If the max extended eax value is high enough to support the
  processor brand string (values 0x80000002 to 0x80000004), then we'll use that
  information to return the brand information. Otherwise, we'll refer back to
  the brand tables above for backwards compatibility with older processors.
  According to the Sept. 2006 Intel Arch Software Developer's Guide, if extended
  eax values are supported,
  then all 3 values for the processor brand string are supported, but we'll test
  just to make sure and be safe. */
    if (max_eax >= 0x80000004) {
        if (max_eax >= 0x80000002) {
            cpuid(0x80000002, eax, ebx, ecx, edx);
            regs2string(eax, ebx, ecx, edx, brandstring);
        }
        if (max_eax >= 0x80000003) {
            cpuid(0x80000003, eax, ebx, ecx, edx);
            regs2string(eax, ebx, ecx, edx, &brandstring[16]);
        }
        if (max_eax >= 0x80000004) {
            cpuid(0x80000004, eax, ebx, ecx, edx);
            regs2string(eax, ebx, ecx, edx, &brandstring[32]);
        }
    } else if (brand > 0) {
        if (brand < 0x18) {
            if (signature == 0x000006B1 || signature == 0x00000F13) {
                strncpy(brandstring, Intel_Other[brand], 48);
            } else {
                strncpy(brandstring, Intel[brand], 48);
            }
        } else {
            strncpy(brandstring, "Reserved\n", 48);
        }
        sprintf(brandstring, "Brand %d - %s", brand, brandstring);
    }
    return brandstring;
}
