#include <input_handler.h>
#include <isr.h>
#include <keyboard.h>
#include <ports.h>
#include <printf.h>
#include <putchar.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <unused.h>

static bool capslock_pressed = false;
static bool numlock_pressed = false;
static bool scrolllock_pressed = false;
static bool shift_pressed = false;

/*
const char *sc_name[] = { "ERROR", "Esc", "1", "2", "3", "4", "5", "6",
        "7", "8", "9", "0", "-", "=", "Backspace", "Tab", "Q", "W", "E",
                "R", "T", "Y", "U", "I", "O", "P", "[", "]", "Enter", "Lctrl",
                "A", "S", "D", "F", "G", "H", "J", "K", "L", ";", "'", "`",
                "LShift", "\\", "Z", "X", "C", "V", "B", "N", "M", ",", ".",
                "/", "RShift", "Keypad *", "LAlt", "Spacebar", "CapsLock", "F1",
                "F2", "F3", "F4", "F5", "F6", "F7", "F8", "F9", "F10",
"NumLock", "ScrollLock", "(keypad) 7", "(keypad) 8", "(keypad) 9",
                "(keypad) -", "(keypad) 4", "(keypad) 5", "(keypad) 6",
                "(keypad) +", "(keypad) 1", "(keypad) 2", "(keypad) 3",
                "(keypad) 0", "(keypad) .", "ERROR", "ERROR", "ERROR", "F11",
"F12"};
*/

static const char sc_ascii[] = {
    '?', '?', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '-',
    '=', '\b', '\t', 'Q', 'W', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P',
    '[', ']', '\n', '^', 'A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L',
    ';', '\'', '`', '?', '\\', 'Z', 'X', 'C', 'V', 'B', 'N', 'M', ',',
    '.', '/', '?', '?', '?', ' ', '?', '?', '?', '?', '?', '?', '?',
    '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?',
    '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?'
};

const uint16_t scmax = sizeof sc_ascii - 1;

static char key_buffer[KEYBUFFERSIZE];

int32_t backspace_limit = -1;

static char to_lower(const char c)
{
    if (c >= 'A' && c <= 'Z')
        return c + 32;
    return c;
}

static void keypad(const uint8_t scancode)
{
    // todo: optimize this function
    if (!numlock_pressed)
        return;
    char c = 0;
    switch (scancode) {
    case KEY0:
        c = '0';
        break;
    case KEY1:
        c = '1';
        break;
    case KEY2:
        c = '2';
        break;
    case KEY3:
        c = '3';
        break;
    case KEY4:
        c = '4';
        break;
    case KEY5:
        c = '5';
        break;
    case KEY6:
        c = '6';
        break;
    case KEY7:
        c = '7';
        break;
    case KEY8:
        c = '8';
        break;
    case KEY9:
        c = '9';
        break;
    case KEYMINUS:
        c = '-';
        break;
    case KEYPLUS:
        c = '+';
        break;
    case KEYDOT:
        c = '.';
        break;
    case KEYASTERISK:
        c = '*';
        break;
    }
    putchar(c);
}

static void append(char *s, const char n)
{
    const size_t len = strlen(s);
    switch (n) {
    case '\n':
        input_handler(s);
        s[0] = '\0';
        backspace_limit = -1;
        break;
    case '\b':
        s[len - 1] = '\0';
        backspace_limit = len;
        break;
    case '\t':
        s[len] = ' ';
        s[len + 1] = ' ';
        s[len + 2] = ' ';
        s[len + 3] = ' ';
        s[len + 4] = '\0';
        break;
    default:
        s[len] = n;
        s[len + 1] = '\0';
        break;
    }
}

static void output(const char c) {
    putchar(c);
    append(key_buffer, c);
}

void keyboard_callback(const registers_t regs) {
    UNUSED(regs);

    // reading scancode
    const uint8_t sc = port_byte_in(0x60);

    // todo: add escape and control functions
    // todo: add f(n) functions

    if (sc == LSHIFTRELEASE || sc == RSHIFTRELEASE)
        shift_pressed = false;
    if (sc > scmax)
        return;
    switch (sc) {
    case CAPSLOCK:
        capslock_pressed = !capslock_pressed;
        break;
    case NUMLOCK:
        numlock_pressed = !numlock_pressed;
        break;
    case SCROLLLOCK:
        scrolllock_pressed = !scrolllock_pressed;
        break;
    case LSHIFT:
    case RSHIFT:
        shift_pressed = true;
        break;
    case LALT:
    case RALT:
    case LCTRL:
    case KEY0:
    case KEY1:
    case KEY2:
    case KEY3:
    case KEY4:
    case KEY5:
    case KEY6:
    case KEY7:
    case KEY8:
    case KEY9:
    case KEYMINUS:
    case KEYPLUS:
    case KEYDOT:
    case KEYASTERISK:
        keypad(sc);
        break;
    default:
        backspace_limit++;
        if (capslock_pressed || shift_pressed) {
            output(sc_ascii[sc]);
        } else {
            output(to_lower(sc_ascii[sc]));
        }

        break;
    }
}
