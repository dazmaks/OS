#include <stdint.h>
#include <timer.h>
#include <unused.h>

uint32_t tick = 0;

void timer_callback(const registers_t regs)
{
    UNUSED(regs);
    tick++;
}
