#include <idt.h>
#include <interrupts.h>
#include <isr.h>
#include <keyboard.h>
#include <mem.h>
#include <ports.h>
#include <printf.h>
#include <stdbool.h>
#include <string.h>
#include <timer.h>

#define sig(x, y) set_idt_gate(x, (uint32_t)y);

isr_t interrupt_handlers[256];

/* Can't do this with a loop because we need the address
 * of the function names */
void isr_install(void)
{
    sig(ISR0, isr0);
    sig(ISR1, isr1);
    sig(ISR2, isr2);
    sig(ISR3, isr3);
    sig(ISR4, isr4);
    sig(ISR5, isr5);
    sig(ISR6, isr6);
    sig(ISR7, isr7);
    sig(ISR8, isr8);
    sig(ISR9, isr9);
    sig(ISR10, isr10);
    sig(ISR11, isr11);
    sig(ISR12, isr12);
    sig(ISR13, isr13);
    sig(ISR14, isr14);
    sig(ISR15, isr15);
    sig(ISR16, isr16);
    sig(ISR17, isr17);
    sig(ISR18, isr18);
    sig(ISR19, isr19);
    sig(ISR20, isr20);
    sig(ISR21, isr21);
    sig(ISR22, isr22);
    sig(ISR23, isr23);
    sig(ISR24, isr24);
    sig(ISR25, isr25);
    sig(ISR26, isr26);
    sig(ISR27, isr27);
    sig(ISR28, isr28);
    sig(ISR29, isr29);
    sig(ISR30, isr30);
    sig(ISR31, isr31);
    // Remap the PIC
    port_byte_out(0x20, 0x11);
    port_byte_out(0xA0, 0x11);
    port_byte_out(0x21, 0x20);
    port_byte_out(0xA1, 0x28);
    port_byte_out(0x21, 0x04);
    port_byte_out(0xA1, 0x02);
    port_byte_out(0x21, 0x01);
    port_byte_out(0xA1, 0x01);
    port_byte_out(0x21, 0x0);
    port_byte_out(0xA1, 0x0);

    // Install the IRQs
    sig(IRQ0, irq0);
    sig(IRQ1, irq1);
    sig(IRQ2, irq2);
    sig(IRQ3, irq3);
    sig(IRQ4, irq4);
    sig(IRQ5, irq5);
    sig(IRQ6, irq6);
    sig(IRQ7, irq7);
    sig(IRQ8, irq8);
    sig(IRQ9, irq9);
    sig(IRQ10, irq10);
    sig(IRQ11, irq11);
    sig(IRQ12, irq12);
    sig(IRQ13, irq13);
    sig(IRQ14, irq14);
    sig(IRQ15, irq15);

    set_idt();
}

char* exception_messages[] = {
    "Division By Zero",
    "Debug",
    "Non Maskable Interrupt",
    "Breakpoint",
    "Into Detected Overflow",
    "Out of Bounds",
    "Invalid Opcode",
    "No Coprocessor",

    "Double Fault",
    "Coprocessor Segment Overrun",
    "Bad TSS",
    "Segment Not Present",
    "Stack Fault",
    "General Protection Fault",
    "Page Fault",
    "Unknown Interrupt",

    "Coprocessor Fault",
    "Alignment Check",
    "Machine Check",
    "Reserved",
    "Reserved",
    "Reserved",
    "Reserved",
    "Reserved",

    "Reserved",
    "Reserved",
    "Reserved",
    "Reserved",
    "Reserved",
    "Reserved",
    "Reserved",
    "Reserved"
};

void isr_handler(const registers_t r)
{
    printf("Interrupt: %d(%s)\n", r.int_no, exception_messages[r.int_no]);
}

void register_interrupt_handler(const uint8_t n, const isr_t handler)
{
    interrupt_handlers[n] = handler;
}

void irq_handler(const registers_t r)
{
    if (r.int_no >= 40)
        port_byte_out(0xA0, 0x20);
    port_byte_out(0x20, 0x20);

    /* Handle the interrupt */
    if (interrupt_handlers[r.int_no] != 0) {
        isr_t handler = interrupt_handlers[r.int_no];
        handler(r);
    }
}

static void init_timer(const uint8_t irq, const uint32_t freq)
{
    register_interrupt_handler(irq, timer_callback);

    const uint32_t divisor = 1193180 / freq;
    const uint8_t low = low_8(divisor);
    const uint8_t high = high_8(divisor);
    /* Send the command */
    port_byte_out(0x43, 0x36); /* Command port */
    port_byte_out(0x40, low);
    port_byte_out(0x40, high);
}

void irq_install(void)
{
    enable_interrupts(true);

    /* IRQ0: timer */
    init_timer(IRQ0, 50);

    /* IRQ1: keyboard */
    register_interrupt_handler(IRQ1, keyboard_callback);
}
