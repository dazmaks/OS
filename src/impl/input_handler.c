#include <color.h>
#include <input_handler.h>
#include <printf.h>
#include <putchar.h>
#include <stdint.h>
#include <string.h>
#include <terminal.h>

#define INPUT(x) !strcmp(input, x)
extern uint32_t tick;

void input_handler(const char* input)
{
    if (INPUT("end")) {
        printf("Stopping the CPU on tick %d\n", tick);
        asm volatile("hlt");
    } else if (INPUT("clear")) {
        for (size_t y = 0; y < VGA_ROW_MAX; y++) {
            for (size_t x = 0; x < VGA_COLUMN_MAX; x++) {
                putentryat(' ', term_color, x, y);
            }
        }
        term_column = 0;
        term_row = 0;
    } else if (INPUT("coltest")) {
        const uint8_t current_color = term_color;
        for (size_t i = 0; i < 16; i++) {
            for (size_t j = 0; j < 16; j++) {
                term_color = get_color(j, i);
                putchar('A');
            }
            term_color = current_color;
            printf(" %d\n", i);
        }
    } else if (INPUT("termstat")) {
        printf("terminal color: %d/255\n"
               "terminal column: %d/%d\n"
               "terminal row: %d/%d\n",
            term_color, term_column, VGA_COLUMN_MAX, term_row, VGA_ROW_MAX);
    } else if (INPUT("tick")) {
        printf("Current tick = %d\n", tick);
    }
    putchar('>');
}
