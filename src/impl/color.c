#include <color.h>

uint8_t get_color(const color_t fg, const color_t bg)
{
    return fg | bg << 4;
}
