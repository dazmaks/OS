#include <interrupts.h>

static bool interrupts_enabled = false;
void toggle_interrupts(void)
{
    interrupts_enabled = !interrupts_enabled;
    asm volatile("sti");
}

void enable_interrupts(const bool status)
{
    if (interrupts_enabled && !status)
        toggle_interrupts();
    if (!interrupts_enabled && status)
        toggle_interrupts();
}
