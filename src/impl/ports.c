#include <ports.h>

uint8_t port_byte_in(const uint16_t port)
{
    register uint8_t result;
    __asm__("in %%dx, %%al"
            : "=a"(result)
            : "d"(port));
    return result;
}

void port_byte_out(const uint16_t port, const uint8_t data)
{
    __asm__ volatile("out %%al, %%dx"
                     :
                     : "a"(data), "d"(port));
}

/*
uint16_t port_word_in (const uint16_t port) {
        uint16_t result;
        __asm__("in %%dx, %%ax" : "=a" (result) : "d" (port));
        return result;
}

void port_word_out (const uint16_t port, const uint16_t data) {
        __asm__ volatile("out %%ax, %%dx" : : "a" (data), "d" (port));
}
*/
