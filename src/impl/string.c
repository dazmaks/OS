#include <string.h>

int strcmp(const char s1[], const char s2[])
{
    int i;
    for (i = 0; s1[i] == s2[i]; i++) {
        if (s1[i] == '\0')
            return 0;
    }
    return s1[i] - s2[i];
}

char* strncpy(char* restrict dst, const char* restrict src, size_t n)
{
    if (n != 0) {
        char* d = dst;

        do {
            if ((*d++ = *src++) == 0) {
                while (--n != 0)
                    *d++ = 0;
                break;
            }
        } while (--n != 0);
    }
    return (dst);
}

size_t strlen(const char* str)
{
    size_t i;
    for (i = 0; str[i]; i++)
        ;
    return i;
}
