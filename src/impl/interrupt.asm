[extern isr_handler]
[extern irq_handler]

%macro isr 1
global isr%1

isr%1:
	cli
	push byte 0
	push byte %1
	jmp isr_common_stub

%endmacro

%macro irq 1
global irq%1

irq%1:
	cli
	push byte %1
	push byte 32+%1
	jmp irq_common_stub

%endmacro

isr_common_stub:
	pusha
	mov ax, ds
	push eax
	mov ax, 0x10
	mov ds, ax
	mov es, ax
	mov fs, ax
	mov gs, ax
	call isr_handler

	pop eax
	mov ds, ax
	mov es, ax
	mov fs, ax
	mov gs, ax
irq_common_stub:
	pusha
	mov ax, ds
	push eax
	mov ax, 0x10
	mov ds, ax
	mov es, ax
	mov fs, ax
	mov gs, ax
	call irq_handler

	pop ebx
	mov ds, bx
	mov es, bx
	mov fs, bx
	mov gs, bx
	popa
	add esp, 8
	sti
	iret

isr 0  ; Divide By Zero Exception
isr 1  ; Debug Exception
isr 2  ; Non Maskable Interrupt Exception
isr 3  ; Int 3 Exception
isr 4  ; INTO Exception
isr 5  ; Out of Bounds Exception
isr 6  ; Invalid Opcode Exception
isr 7  ; Coprocessor Not Available Exception
isr 8  ; Double Fault Exception (With Error Code!)
isr 9  ; Coprocessor Segment Overrun Exception
isr 10 ; Bad TSS Exception (With Error Code!)
isr 11 ; Segment Not Present Exception (With Error Code!)
isr 12 ; Stack Fault Exception (With Error Code!)
isr 13 ; General Protection Fault Exception (With Error Code!)
isr 14 ; Page Fault Exception (With Error Code!)
isr 15 ; Reserved Exception
isr 16 ; Floating Point Exception
isr 17 ; Alignment Check Exception
isr 18 ; Machine Check Exception
isr 19 ; Reserved
isr 20 ; Reserved
isr 21 ; Reserved
isr 22 ; Reserved
isr 23 ; Reserved
isr 24 ; Reserved
isr 25 ; Reserved
isr 26 ; Reserved
isr 27 ; Reserved
isr 28 ; Reserved
isr 29 ; Reserved
isr 30 ; Reserved
isr 31 ; Reserved

irq 0
irq 1
irq 2
irq 3
irq 4
irq 5
irq 6
irq 7
irq 8
irq 9
irq 10
irq 11
irq 12
irq 13
irq 14
irq 15