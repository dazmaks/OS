#include <cursor.h>
#include <stddef.h>
#include <string.h>
#include <terminal.h>

static void scroll(void)
{
    // Scrolling
    for (size_t y = 0; y < VGA_ROW_MAX; y++) {
        for (size_t x = 0; x < VGA_COLUMN_MAX; x++) {
            term_buffer[POSITION] = term_buffer[(POSITION) + VGA_COLUMN_MAX];
        }
        putentryat(' ', term_color, y, VGA_COLUMN_MAX - 1);
    }
}

static void scroll_check(void)
{
    if (term_row >= VGA_ROW_MAX) {
        scroll();
        --term_row;
    }
}

static void newline_check(void)
{
    if (++term_column >= VGA_COLUMN_MAX) {
        term_column = 0;
        ++term_row;
        scroll_check();
    };
}

extern int32_t backspace_limit;

void putchar(const char c)
{
    switch (c) {
    case '\n':
        term_column = 0;
        ++term_row;
        scroll_check();
        break;
    case '\t':
        term_column += 4;
        newline_check();
        break;
    case '\b':
        if (backspace_limit > 1) {
            if (--term_column == -1) {
                term_column = VGA_COLUMN_MAX;
                if (--term_row == -1) {
                    term_row = 0;
                }
            }
            putentryat(' ', term_color, term_column, term_row);
            update_cursor(term_column - 1, term_row);
        }
        break;
    default:
        putentryat(c, term_color, term_column, term_row);
        newline_check();
        break;
    }
    update_cursor(term_column, term_row);
}
