[bits 32]

VIDEO_MEMORY equ 0xB8000
WHITE_OB_BLACK equ 0x0F

print_string_pm:
	pusha
	mov edx, VIDEO_MEMORY

print_string_pm_loop:
	mov al, [ebx]
	mov ah, WHITE_OB_BLACK

	cmp al, 0
	je print_string_pm_done

	mov [edx], ax
	inc ebx
	inc edx
	inc edx

	jmp print_string_pm_loop

print_string_pm_done:
	popa
	ret
