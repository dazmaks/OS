#include <color.h>
#include <cpuid.h>
#include <cursor.h>
#include <input_handler.h>
#include <isr.h>
#include <printf.h>
#include <putchar.h>
#include <string.h>
#include <terminal.h>

void kernel_early(void)
{
    term_row = 1;
    term_column = 0;
    term_color = 15; // bg-black fg-white
    isr_install();
    irq_install();
    enable_cursor(12, 12);
    printf("%s printf!\n"
           "Kernel compiled at %s(%s) with %s compiler\n"
           "CPU Brand: %s\n",
        __FILE__, __DATE__, __TIME__, __VERSION__, get_brandstring());
    input_handler("termstat");
}
