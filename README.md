# OS
## Setting up
```
sudo apt install make git nasm build-essential
```
# Cloning and Building
```
git clone https://github.com/dazmaks/OS
cd OS

make
```

## TODO
- [x] Fix backspace
- [ ] Add multitasking
- [ ] Add filesystem
- [ ] Add tcc
- [ ] Rewrite bootloader in fasm
- [ ] Add basic interpreter
- [ ] Fix -Os compilation runtime issues ( cpuid )

## resources used
- https://www.osdev.org/
- https://github.com/cfenollosa/os-tutorial
